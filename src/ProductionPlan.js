// import React, {useState} from "react";
import React from "react";
// import axios from 'axios'

import "./styles.css";

import '@progress/kendo-theme-default/dist/all.css';
import {process} from '@progress/kendo-data-query';
import {Grid, GridColumn, GridToolbar} from '@progress/kendo-react-grid';
import {DropDownList} from '@progress/kendo-react-dropdowns';
import {Window} from '@progress/kendo-react-dialogs';
import {ExcelExport} from '@progress/kendo-react-excel-export';
import {GridPDFExport} from '@progress/kendo-react-pdf';
import {Dialog, DialogActionsBar} from '@progress/kendo-react-dialogs';
import {Popup} from '@progress/kendo-react-popup';
import {Menu, MenuItem} from '@progress/kendo-react-layout';
import { Renderers } from './renderers.js';

const requsername = 'admin';
const URL = 'ws://localhost:8000/ws/timeline/' + requsername;
// const URL = 'ws://' + window.location.host + '/ws/timeline/' + requsername;


export default class App extends React.Component {
    constructor(props) {
        super(props);
        const dataState = {
            skip: 0,
            take: 20,
            sort: [
                {field: 'orderDate', dir: 'desc'}
            ],
            group: [
                {field: 'customerID'}
            ]
        };

        this.state = {
            tasks: {},
            keys: {},
            plines: {},
            finalprod: {},
            sschema: {},
            editID: null,
            res: {},
            loaded: false,
            visible: false,
            open: false,
            dropdownlistPLine: null,
            dropdownlistFinProd: null,
            dropdownlistSSchema: null,
            gridDataState: {
                sort: [
                    {field: "ProdLine", dir: "asc"}
                ],
                page: {skip: 0, take: 10}
            },
            windowVisible: false,
            gridClickedRow: {},
            dataState: dataState,
            hasErrors: false,
            Error: '',
            // ws: new WebSocket(URL,)
        }
        this.toggleDialog = this.toggleDialog.bind(this);
        this.updateClicked = this.updateClicked.bind(this);
    }

    blurTimeoutRef;
    menuWrapperRef;

    toggleDialog() {
        this.setState({
            visible: !this.state.visible
        });
    }

    cellRender = (trElement, dataItem) => {
        const trProps = {
            ...trElement.props,
            onContextMenu: (e) => {
                e.preventDefault();
                this.handleContextMenuOpen(e, dataItem.dataItem);
            }
        };
        return React.cloneElement(trElement, {...trProps}, trElement.props.children);
    }

    rowRender = (trElement, dataItem) => {
        const trProps = {
            ...trElement.props,
            onContextMenu: (e) => {
                e.preventDefault();
                this.handleContextMenuOpen(e, dataItem.dataItem);
            }
        };
        return React.cloneElement(trElement, {...trProps}, trElement.props.children);
    }

    handleContextMenuOpen = (e, dataItem) => {
        this.field = e.field;
        this.dataItem = dataItem;
        this.dataItemIndex = this.state.tasks.findIndex(p => (p.rowid === this.dataItem.rowid));
        this.offset = {left: e.clientX, top: e.clientY};
        this.setState({
            open: true
        });
    }


    handlePLineDropDownChange = (e) => {
        let newDataState = {...this.state.gridDataState}
        if (e.target.value.id !== null) {
            newDataState.filter = {
                logic: 'and',
                filters: [{field: 'id', operator: 'eq', value: e.target.value.id}]
            }
            newDataState.skip = 0
        } else {
            newDataState.filter = []
            newDataState.skip = 0
        }
        this.setState({
            dropdownlistPLine: e.target.value.pldesc,
            gridDataState: newDataState
        });
    }

    handleFinalProdDropDownChange = (e) => {
        let newDataState = {...this.state.gridDataState}
        if (e.target.value.id !== null) {
            newDataState.filter = {
                logic: 'and',
                filters: [{field: 'id', operator: 'eq', value: e.target.value.id}]
            }
            newDataState.skip = 0
        } else {
            newDataState.filter = []
            newDataState.skip = 0
        }
        this.setState({
            dropdownlistFinProd: e.target.value.code,
            gridDataState: newDataState
        });
    }

    handleShiftSchemaDropDownChange = (e) => {
        let newDataState = {...this.state.gridDataState}
        if (e.target.value.id !== null) {
            newDataState.filter = {
                logic: 'and',
                filters: [{field: 'id', operator: 'eq', value: e.target.value.id}]
            }
            newDataState.skip = 0
        } else {
            newDataState.filter = []
            newDataState.skip = 0
        }
        this.setState({
            dropdownlistSSchema: e.target.value.id,
            gridDataState: newDataState
        });
    }

    handleGridDataStateChange = (e) => {
        this.setState({gridDataState: e.data});
    }

    async componentDidMount() {
        console.log('Verion 1.3', new Date());
        await fetch('http://127.0.0.1:8000/jsonprodplan', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => this.setState({
                loaded: true,
                res: res,
                tasks: res.tasks,
                keys: res.keys,
                plines: res.plines,
                finalprod: res.finalprod,
                sschema: res.sschema,
            }))
            .catch(error => {
                this.setState({
                    Error: error.toString(),
                    hasErrors: true,
                    loaded: false,
                });
                console.log(error);
            });
        console.log('res', this.state.res)
        /*
                this.state.ws.onopen = () => {
                    // on connecting, do nothing but log it to the console
                    console.log('connected')
                };

                this.state.ws.onmessage = evt => {
                    // on receiving a message, add it to the list of messages
                    console.log('in onmessage... ');
                    const message = JSON.parse(evt.data);
                    // console.log(message.timeline);
                    // console.log(JSON.parse(message.timeline));
                    const tld = JSON.parse(message.timeline).prodlinetasks;
                    // console.log('in onmessage... tld count = ', tld.count);
                    this.setState({tldata: tld});
                };

                this.state.ws.onclose = () => {
                    console.log('disconnected');
                    // automatically try to reconnect on connection loss
                    this.setState({ws: new WebSocket(URL)});
                }
        */
    }

    handleGridRowClick = (e) => {
        this.setState({
            windowVisible: true,
            gridClickedRow: e.dataItem
        });
    }

    closeWindow = (e) => {
        this.setState({
            windowVisible: false
        });
    }

    _pdfExport;
    exportExcel = () => {
        this._export.save();
    }

    _export;
    exportPDF = () => {
        this._pdfExport.save();
    }

//                            <GridColumn field="id" title="ID"/>
    rowClick = (event) => {
        console.log(event);
        this.setState({
            editID: event.dataItem.rowid
        });
    };

    closeEdit = (event) => {
        console.log(event);
        if (event.target === event.currentTarget) {
            this.setState({editID: null});
        }
    };

    addRecord = () => {
        const {tasks} = this.state;
        if (this.state.dropdownlistPLine === null || this.state.dropdownlistFinProd === null || this.state.dropdownlistSSchema === null) {
            this.toggleDialog()
        } else {
            const newRecord = {
                rowid: tasks.length,
                ProdLine: this.state.dropdownlistPLine,
                FinalProduct: this.state.dropdownlistFinProd,
                sschema: this.state.dropdownlistSSchema,
            };

            this.setState({
                tasks: [newRecord, ...tasks],
                editID: newRecord.rowid
            });
        }
        console.log('New tasks', this.state.tasks)
    };

    async updateClicked() {
        await fetch('http://127.0.0.1:8000/jsonprodplan', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(this.state.tasks)
            }
        )
            .then(resp => resp.json())
            .catch(error => console.log(error));
    };

    itemChange = (event) => {
        const inEditID = event.dataItem.rowid;
        const data = this.state.tasks.map(item => {
                if (item.rowid === inEditID) {
                    {
                        item[event.field] = event.value
                    }
                    item['TotPipes'] = 0;
                    item['TotKg'] = 0;
                    const otherrows = this.state.tasks.map(therow => {
                        if (therow.rowid != inEditID) {
                            if (therow['ProdLine'] === event.dataItem.ProdLine) {
                                if (event.value > 0 || isNaN(event.value)) {
                                    therow[event.field] = 0 + '';
                                    let i = 0;
                                    therow['TotPipes'] = 0 + '';
                                    therow['TotKg'] = 0 + '';
                                    this.state.keys.forEach(element => {
                                        if (i > 8) {
                                            let ele = element["field"];
                                            if (!(isNaN(therow[ele]) || therow[ele] != '')) {
                                                therow['TotPipes'] += parseInt(therow[ele]);
                                                therow['TotKg'] += (parseInt(therow[ele]) * therow['Weight']);
                                            }
                                        }
                                        i++;
                                    });
                                }
                            }
                        }
                    });
                    let i = 0;
                    this.state.keys.forEach(element => {
                        if (i > 8) {
                            let ele = element["field"];
                            console.log(ele,item[ele])
                            let x = item[ele]
                            if (!isNaN(item[ele])) {
                                item['TotPipes'] += parseInt(item[ele]);
                                item['TotKg'] += Math.round(parseInt(item[ele]) * item['Weight']);
                            }
                        }
                        i++;
                    });
                }

            }
            // item.rowid === inEditID ? {...item, [event.field]: event.value} : item
        );
        this.setState({data});
    };
    handleOnSelect = (e) => {
        switch (e.item.text) {
            case "Move Up":
                this.handleMoveUp();
                break;
            case "Move Down":
                this.handleMoveDown();
                break;
            case "Delete":
                this.handleDelete();
                break;
            default:
        }
        this.setState({
            open: false
        })
    }

    handleMoveUp = () => {
        let data = [...this.state.tasks];
        if (this.dataItemIndex !== 0) {
            data.splice(this.dataItemIndex, 1);
            data.splice(this.dataItemIndex - 1, 0, this.dataItem);
            this.setState({tasks: data});
        }
    }

    handleMoveDown = () => {
        let data = [...this.state.tasks];
        if (this.dataItemIndex < this.state.tasks.length) {
            data.splice(this.dataItemIndex, 1);
            data.splice(this.dataItemIndex + 1, 0, this.dataItem);
            this.setState({tasks: data});
        }
    }

    handleDelete = () => {
        let data = [...this.state.tasks];
        data.splice(this.dataItemIndex, 1);
        this.setState({
            tasks: data
        });
    }


    onFocusHandler = () => {
        clearTimeout(this.blurTimeoutRef);
        this.blurTimeoutRef = undefined;
    };

    onBlurTimeout = () => {
        this.setState({
            open: false
        });

        this.blurTimeoutRef = undefined;
    };

    onBlurHandler = event => {
        clearTimeout(this.blurTimeoutRef);
        this.blurTimeoutRef = setTimeout(this.onBlurTimeout);
    };


    onPopupOpen = () => {
        this.menuWrapperRef.querySelector('[tabindex]').focus();
    };


    render() {
        // function fixupColumn({index, field, heading}) {
        //     console.log(index, field, heading)
        //     switch (index) {
        //         case 'info':
        //             return <GridColumn field={field} title={heading} width={0}/>;
        //         default:
        //             return <GridColumn field={field} title={heading}/>;
        //     }
        // }
        //
        if (this.state.hasErrors) {
            return <h1>Error: {this.state.Error}</h1>
        } else if (this.state.loaded) {
            return (
                <div className="App">
                    <h1>SPSM Production Planning</h1>
                    <p>
                        &nbsp; <strong>Select Prod Line:</strong>
                        {/*&nbsp; Selected PLine : <strong>{this.state.dropdownlistPLine}</strong>*/}
                        <DropDownList
                            data={this.state.plines}
                            dataItemKey="id"
                            textField="pldesc"
                            onChange={this.handlePLineDropDownChange}
                        />
                        &nbsp; <strong>Select Final Product: </strong>
                        {/*&nbsp; Selected Final Product: <strong>{this.state.dropdownlistFinProd}</strong>*/}
                        <DropDownList
                            data={this.state.finalprod}
                            dataItemKey="id"
                            textField="code"
                            onChange={this.handleFinalProdDropDownChange}
                        />
                        &nbsp; <strong>Select Shift Schema: </strong>
                        {/*&nbsp; Selected Final Product: <strong>{this.state.dropdownlistFinProd}</strong>*/}
                        <DropDownList
                            data={this.state.sschema}
                            dataItemKey="id"
                            textField="ssdesc"
                            onChange={this.handleShiftSchemaDropDownChange}
                        />
                    </p>
                    <div>
                        <button className="k-button" onClick={this.toggleDialog}>Open Dialog</button>
                        {this.state.visible && <Dialog title={"Fields required"} onClose={this.toggleDialog}>
                            <p style={{margin: "25px", textAlign: "center"}}>Please select a Production Line and a Final Product before adding a new row</p>
                            <DialogActionsBar>
                                <button className="k-button" onClick={this.toggleDialog}>OK</button>
                            </DialogActionsBar>
                        </Dialog>}
                    </div>
                    <Popup
                        offset={this.offset}
                        show={this.state.open}
                        open={this.onPopupOpen}
                        popupClass={'popup-content'}
                    >
                        <div
                            onFocus={this.onFocusHandler}
                            onBlur={this.onBlurHandler}
                            tabIndex={-1}
                            ref={el => (this.menuWrapperRef = el)}
                        >
                            <Menu vertical={true} style={{display: 'inline-block'}} onSelect={this.handleOnSelect}>
                                <MenuItem text="Move Up"/>
                                <MenuItem text="Move Down"/>
                                <MenuItem text="Delete"/>
                            </Menu>
                        </div>
                    </Popup>
                    <ExcelExport
                        data={this.state.tasks}
                        ref={(exporter) => {
                            this._export = exporter;
                        }}
                    >
                        <Grid
                            // onRowClick={this.handleGridRowClick}
                            style={{height: '420px'}}
                            data={this.state.tasks.map((item) =>
                                ({...item, inEdit: item.rowid === this.state.editID})
                            )}
                            editField="inEdit"

                            onRowClick={this.rowClick}
                            onItemChange={this.itemChange}
                            cellRender={Renderers.cellRender}
                        >
                            {
                                this.state.keys.map((task, index) => {
                                        switch (index) {
                                            case 0 :
                                                return <GridColumn field={task.field} title={task.heading} width={0}/>;
                                            case 1 :
                                                return <GridColumn locked field={task.field} title={task.heading} width={90} editable={false}/>;
                                            case 2 :
                                                return <GridColumn locked field={task.field} title={task.heading} width={110} editable={false}/>;
                                            case 3 :
                                            case 4 :
                                            case 6 :
                                            case 7 :
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={85}/>;
                                            case 5 :
                                                // return <GridColumn locked field={task.field} title={task.heading} editable={false} width={85}/>;
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={85} cell={cellWithBackGround}/>;
                                            case 8 :
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={0}/>;
                                            default  :
                                                return <GridColumn field={task.field} title={task.heading} editable={true}  width={100}/>;
                                                // return <GridColumn field={task.field} title={task.heading} filter="numeric" editable={true} editor="numeric" width={100}/>;
                                        }
                                        // if (index == 0) {
                                        //     return <GridColumn field={task.field} title={task.heading} width={0}/>;
                                        // }
                                    }
                                )
                            }
                            <GridToolbar>
                                <button title="Add new" className="k-button k-primary" onClick={this.addRecord}>
                                    Add new
                                </button>
                                <button
                                    title="Export to Excel"
                                    className="k-button k-primary"
                                    onClick={this.exportExcel}
                                >
                                    Export to Excel
                                </button>
                                &nbsp;
                                <button className="k-button k-primary" onClick={this.exportPDF}>Export to PDF</button>
                                <button className="k-button k-primary" onClick={this.updateClicked}>Update</button>
                            </GridToolbar>

                        </Grid>
                    </ExcelExport>
                    <GridPDFExport
                        ref={(element) => {
                            this._pdfExport = element;
                        }}
                        margin="1cm">
                        {<Grid data={process(this.state.tasks, {skip: this.state.dataState.skip, take: this.state.dataState.take})}>
                            {
                                this.state.keys.map((task, index) => {
                                        switch (index) {
                                            case 0 :
                                                return <GridColumn field={task.field} title={task.heading} width={0}/>;
                                            case 1 :
                                                return <GridColumn locked field={task.field} title={task.heading} width={90} editable={false}/>;
                                            case 2 :
                                                return <GridColumn locked field={task.field} title={task.heading} width={110} editable={false}/>;
                                            case 3 :
                                            case 4 :
                                            case 5 :
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={85} cell={cellWithBackGround}/>;
                                            case 6 :
                                            case 7 :
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={85}/>;
                                            case 8 :
                                                return <GridColumn locked field={task.field} title={task.heading} editable={false} width={0}/>;
                                            default  :
                                                return <GridColumn field={task.field} title={task.heading} filter="numeric" editable={true} editor="numeric" width={100}/>;
                                        }
                                        // if (index == 0) {
                                        //     return <GridColumn field={task.field} title={task.heading} width={0}/>;
                                        // }
                                    }
                                )
                            }
                        </Grid>}
                    </GridPDFExport>

                    {this.state.windowVisible &&
                    <Window
                        title="Production Plan"
                        onClose={this.closeWindow}
                        height={250}>
                        <dl style={{textAlign: "left"}}>
                            <dt>Production Line</dt>
                            <dd>{this.state.gridClickedRow.ProdLine}</dd>
                            <dt>Product ID</dt>
                            <dd>{this.state.gridClickedRow.rowid}</dd>
                        </dl>
                    </Window>
                    }
                </div>
            );
        } else {
            return <h1> No Pipes {this.state.actid}</h1>
        }
    }
}

class checkboxColumn extends React.Component {
    render() {
        return (
            <td>
                <input type="checkbox" checked={this.props.dataItem[this.props.field]} disabled="disabled"/>
            </td>
        );
    }
}

class cellWithBackGround extends React.Component {
    render() {
        const stockfree = this.props.dataItem.StockFree < 0;

        const icon = stockfree ?
            <span className="k-icon k-i-sort-desc-sm"/> :
            <span className="k-icon k-i-sort-asc-sm"/>;

        const style = {
            backgroundColor: stockfree ?
                "rgb(243, 23, 0, 0.32)" :
                "rgb(55, 180, 0,0.32)"
        };

        return (
            <td style={style}>
                {this.props.dataItem[this.props.field]} {icon}
            </td>
        );
    }
}
